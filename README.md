[ENG](README.md) | [PL](README.pl.md)

# Szostki - client-server card game

This project is my first card game (and game in general) created for mobile devices. It's using client-server architecture in Java/Kotlin and Google Firebase. 
I created it to learn some new stuff and to expand my portfolio as a freelancer.

## Table of contents
* [General info](#general-info)
* [Game rules and gameplay](#game-rules-and-gameplay)
* [Technologies](#technologies)
* [Setup](#setup)
* [Sources and inspirations](#sources-and-inspirations)
* [License](#license)


## General info
The game is called "Szóstki", which means "Sixes" (plural of "Six") in polish. 

## Game rules and gameplay

### Rules
Basic goal of the game is to collect **as little** points as possible. To do so there are few important rules:

- The game is suitable for 2-6 players, however the most exciting gameplay should happen when there are more than 3 of them.
- Full card deck is used (all 55 cards including Jokers).
- Gameplay is divided into 6 rounds.
- Each round consists of turns. The amount of turns depends on the players actions.
- Each card have point values:

| Card | Value |
| ------ | ------ |
| Joker | -5 |
| A | 0 |
| K | 1 |
| 2-10 | 2-10 |
| J | 11 |
| Q | 12 |

- Two cards in a column with the same value neutralise their values (both of them are worth 0 points).
- Four cards that create a square give -25 points.
- Practice makes perfect :)

### Gameplay
Those are the basic rules of the game but... **How do you play it after all?** 

Gameplay may seem quite complicated at first, but it becomes brighter after time. 
The best way to understand it is to play with your friends having fun :)

Those are the rules of the "real-life" version of the game:

0. You prepare score table that looks like this:
 
| | Player 1 | Player 2 | Player 3 |  Player 4 | Player 5 | Player 6 |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| I | | | | | | |
| II | | | | | | |
| III | | | | | | |
| IV | | | | | | |
| V | | | | | | |
| VI | | | | | | |
| Total | | | | | | |

*(Rows represent each round and you will write down players score in according columns.)*

1. You choose first player randomly.
2. Person on the left is shuffling the deck.
3. The same person is now dealing the cards between players - 6 cards each.
4. Each player is distributing his/her cards into 2 rows (3 columns each) in front of him/her.
5. Each player is flipping 2 of his cards. The configuration can be any. Eg.:
6. The cards left from the deal create **covered stack**. First card from the stack is being flipped on the uncovered side and put next to the covered stack. It will create **uncovered stack**.
7. The first player can now:

    a) as
8. After first player's turn the game continues clockwise. Next players have exactly the same possibilities as those stated in point 7.
9. The round is going on until one of the players uncover all his/her cards. After that each player has one more turn to use.
10. Everyone uncovers their left covered cards and write down points collected during the round (according to the rules).

Example 1:

Example 2:

11. Next round begins. The first player is now next person counting clockwise (the same that was shuffling and dealing cards before).
12. You play until 6th round is over. All points are summed and person with the least points is the winner. The table can for example look like this
 
| | Player 1 | Player 2 | Player 3 |  Player 4 | Player 5 | Player 6 |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| I | 13| 23 | 1 | 7 | -1 | 17 |
| II | 29 | 0 | 5| 2 | 12 | 13 |
| III | -7 | -5 | 2 | 15 | 16 | 4|
| IV | 0 | -1 | 32 | 9 | 3 | 7 |
| V | 9 | 3 | 13 | -5 | 20 | 1 |
| VI | 1 | 2 | 29 | 17 | 2 | 4 |
| **Total** | **45** | **22** | **82** | **45** | **52** | **46** |

*(In this case Player 2 is the winner)*

## Technologies
:)

## Setup
:)


## Sources and inspirations
:)

## License
This software is licensed under MIT license. See more on [LICENSE.md](LICENSE.md)